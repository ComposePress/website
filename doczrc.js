export default {
	base: '/',
	ignore: ['README.md'],
	title: 'ComposePress',
	description: 'A fast, modern, modular, extendable OOP WordPress plugin framework for the modern age',
	menu: [
		'What is ComposePress?',
		'Quick Start',
		'Concepts'
	]
}
